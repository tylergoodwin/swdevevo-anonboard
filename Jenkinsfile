pipeline {
  agent any
  stages {

    stage ('Build') {
      steps {
        sh 'chmod +x bin/*'
        sh 'docker-compose -f docker-compose.production.yml build'
      }
    }
    stage ('Test') {
      steps {
        sh 'docker-compose -f docker-compose.production.yml run -e RAILS_ENV=development app bin/rails db:create'
        sh 'docker-compose -f docker-compose.production.yml run -e RAILS_ENV=development app bin/rails db:migrate'
        sh 'docker-compose -f docker-compose.production.yml run -e RAILS_ENV=development app bin/rails test'
      }
    }

    stage ('Archive') {
      steps {
        sh 'rm archive.zip || echo "no file found"'
        zip archive: true, dir: '', zipFile: 'archive.zip'
      }
    }

    stage ('Deploy to Staging') {
      when { branch 'development' }
      steps {
        sh 'echo Running deploy to staging'
        withCredentials([string(credentialsId: 'jenkins-staging-password', variable: 'staging_password')]){
          sh "sshpass -p ${staging_password} scp -o StrictHostKeyChecking=no archive.zip vagrant@staging:~/app/"
          sh "sshpass -p ${staging_password} ssh -o StrictHostKeyChecking=no vagrant@staging 'cd ~/app && unzip -o archive.zip && chmod +x deploy.sh && ./deploy.sh'"
        }
      }
    }

    stage ('Deploy to Production') {
      when { branch 'master' }
      steps {
        sh 'echo Running deploy to production'
        withCredentials([string(credentialsId: 'jenkins-production-password', variable: 'password')]){
          sh "sshpass -p ${password} ssh -o StrictHostKeyChecking=no vagrant@production 'export USE_INFLUX=true'"
          sh "sshpass -p ${password} scp -o StrictHostKeyChecking=no archive.zip vagrant@production:~/app/"
          sh "sshpass -p ${password} ssh -o StrictHostKeyChecking=no vagrant@production 'cd ~/app && unzip -o archive.zip && chmod +x deploy.sh && ./deploy.sh'"
        }
      }
    }
  }
}